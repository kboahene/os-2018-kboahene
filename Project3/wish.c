#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h> 
#include <fcntl.h>
#include <pthread.h>

#define TOKE " \t\r\n\a"

// change path
void pathFunc(char *array, char * path[], int i, size_t buffsize);


// change directory 
void cdFunc(char *array, size_t buffsize);

// run exec 2
void excFunc(char *array, char * path[], int i, size_t buffsize);

// run exec
int ex2Func(char *array, char * path[], int i, size_t buffsize);

void redirec(char *array, char * path[], int i, size_t buffsize);


void  *parallel (void *data );


int main(int argc, char *argv[]) {

   ssize_t characters = 0;
    size_t buffsize = 1000;
    char *buffer = NULL;
    char *bin[] = {"/bin/","/usr/bin"};
    int i = 0;

    // check to run either batch or interactive mode
    if(argv[1] != NULL) {
         FILE *cat;


    // exit for the loop
    if(strncmp(argv[1],"exit",4) == 0)  {
            exit(0);
        }

    else if( (cat= fopen(argv[1], "r")) == NULL) {

        char error_message[30] = "An error has occurred\n";
        write(STDERR_FILENO, error_message, strlen(error_message));   // exits if file does not exist
    	fclose(cat);
		return(0); 

        }

 
    else {
        cat= fopen(argv[1], "r");

        while( (characters = getline(&buffer,&buffsize,cat)) >= 0 ) {
             write(STDOUT_FILENO,buffer,strlen(buffer));

            //run path function
            if(strncmp(buffer,"path", 4)== 0) {
                pathFunc(buffer,bin,i,buffsize);
        
            }

            //run cd function
            else if(strncmp(buffer,"cd", 2) == 0 ) {
                cdFunc(buffer,buffsize);
            }


            else if(strncmp(buffer,"exit",4) == 0)  {
                exit(0);
            }


            

            else {
                //interactive mode
                excFunc(buffer,bin,i,buffsize);
                free(buffer);
            }

    

    }
   
   
    }

    return 0;
    
    }

    // interactive mode
    else {
        
        printf("wish> ");
        int loop = 0;

        while(loop == 0) {

        ssize_t characters = 0;
        size_t buffsize = 1000;
        char *buffer = NULL;
        characters = getline(&buffer,&buffsize,stdin);

        // run path function
        if(strncmp(buffer,"path", 4)== 0) {
            pathFunc(buffer,bin,i, buffsize);
             printf("wish> ");
     
        }

        else if (strstr(buffer,">")!= NULL) {
            redirec(buffer,bin,i, buffsize);
             printf("wish> ");

        }


        else if(strstr(buffer,"&")!= NULL) {
            int buffer_length = strlen(buffer);
            buffer[buffer_length-1] = 0;
            char* str[100];
            char* token;
            int rc, a;
            token = strtok(buffer, "&");
            int post = 0;
            while(token != NULL) {
                str[post] = token;
               // printf("%s\n",str[post]);
                token = strtok(NULL, "&");
                post = post + 1;
                }

       
            pthread_t threads[post];
            

            for (int u =0 ; u < post; ++u) {
            pthread_create(&threads[u], NULL, parallel, str[u]);
                
        }
                
             for (int u =0 ; u <post; ++u) {
            pthread_join(threads[u], NULL);
                }


             printf("wish> ");
        }

        // run cd function
        else if(strncmp(buffer,"cd", 2) == 0 ) {
            cdFunc(buffer, buffsize);
             printf("wish> ");
        }

        // run exit function
        else if(strncmp(buffer,"exit",4) == 0)  {
            exit(0);
        }

        //interactive modes
        else {
            char* str[100];
            char *token;
            buffer[strlen(buffer) - 1] = 0;
            token = strtok(buffer, " ");
            int post = 0;
            while(token != NULL ) {
                str[post] = token;
                token = strtok(NULL, " ");
                post = post + 1;

            }
           str[post] = NULL;

            int fot = fork();

            if(fot == 0 ) {
                char b[50];
                strcat(b,bin[i]);
                strcat(b,str[0]);
                int accs = access(b, X_OK);
                if (accs == 0) {
                    execv(b,str);
                }
                else{
                //     char check[15];
                //     strcat(check,"/usr/bin/");
                //     strcat(check,str[0]);
                //    int cc=  access(check,X_OK);
                //    if(cc == 0) {
                //        execv(check,str);
                //    }
                char error_message[30] = "An error has occurred\n";
                write(STDERR_FILENO, error_message, strlen(error_message)); 
                  
                return 0;
                }
                 
            }
            else 
            {
                wait(NULL);
                printf("wish> ");
            }

        }

    loop = 0;
   


    }


    }







}


void pathFunc(char *array, char * path[], int i, size_t buffsize) {
    char* str[] = {};
    char *token;
    write(STDOUT_FILENO,array,strlen(array));
    token = strtok(array, " ");
    int post = 0;
    while(token != NULL ) {
        str[post] = token;
        token = strtok(NULL, " ");
        post = post + 1;
        }

    for(int t = 1; t < post; t++) {
        if(strncmp(str[t],"/bin",4) == 0 ) {
            path[i] = "/bin/";
            printf("path changed to %s \n",str[t]);
        }
        else if(strncmp(str[t],"/usr/bin",8)  == 0) {
                i = 1;
                printf("path changed to %s \n",str[t]);
                }
            else {
                path[i] = str[t];     
                }       
            }
}

//the directory in 
void cdFunc(char *array, size_t buffersize) {

    char* str[] = {};
    char *token;
    write(STDOUT_FILENO,array,strlen(array));
    token = strtok(array, " ");
    int post = 0;
    while(token != NULL ) {
        str[post] = token;
        token = strtok(NULL, " ");
        post = post + 1;
        }

        char * ps;
        int st;
    if((ps = strchr(str[1], '\n')) != NULL) {
        *ps = '\0';

        st = chdir(str[1]);
        if(st != 0) {
            char error_message[30] = "An error has occurred\n";
    write(STDERR_FILENO, error_message, strlen(error_message)); 
        }
        else{
            printf(" directory changed \n");
        }
 
        }
}


void excFunc(char * array, char * path[], int i, size_t buffsize) {
     char* str[100];
    char *token;

    token = strtok(array, TOKE);
    int post = 0;
    while(token != NULL ) {
        str[post] = token;
        token = strtok(NULL, TOKE);
        post = post + 1;
        }
        
        char b[50];
        strcat(b,path[i]);
        strcat(b,str[0]);
        int accs = access(b, X_OK);
        if (accs == 0) {
            execv(b,str);
            }
      else{
                    char check[15];
                    strcat(check,"/usr/bin/");
                    strcat(check,str[0]);
                   int cc=  access(check,X_OK);
                   if(cc == 0) {
                       execv(check,str);
                   }

            }
} 


void redirec(char *array, char * path[], int i, size_t buffsize) {


        char  * str[100];
        
        int tsd = 0;
        char *token;
        char  *filename = NULL;
       
        
        token = strtok(array, " ");
       
        while(token != NULL ) {
            
            if(strncmp(token,">",1) != 0 && strstr(token,".txt")== NULL ) {
                  str[tsd] = token;
                    tsd = tsd + 1; 
              }
            else if(strstr(token,".txt")!= NULL) {
     
                filename = token;
                
            }

            token = strtok(NULL, " ");

        }

        filename[strlen(filename)-1]=0;

       if (fork() == 0) {

        int file = open( filename,  O_RDWR | O_CREAT | O_TRUNC , S_IRUSR | S_IWUSR );
                        if (file < 0) {
                    char error_message[30] = "An error has occurred\n";
                    write(STDERR_FILENO, error_message, strlen(error_message)); 
            }
       
       
        dup2(file,1);
        close(file);

        char b[50];
        strcat(b,path[i]);
        strcat(b, str[0]);
        
        int accs = access(b, X_OK);
        if (accs == 0) {
            execv(b,str);      
            }
}

}


void  *parallel (void *data ) {
       
     char  *rayy;
      char *token;
     
    rayy =  (char *)  data;

    char * cmdsplit[] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}; 
    token = strtok(rayy, " ");
                
                int w = 0;
                while(token != NULL) {
                cmdsplit[w] = token;
                token = strtok(NULL, " ");
                w = w + 1;
                }

    

    

        char b[50];
        strcat(b,"/bin/");

        strcat(b,cmdsplit[0]);

        int accs = access(b, X_OK);


     
   int cprocess = fork();

   
        if(cprocess < 0) {

            exit(1);
        }
        
   

       else if( cprocess ==0) {
        if (accs == 0) {
        execv(b,cmdsplit);
        wait(NULL);
    
        }

        else{
             char check[15];
             strcat(check,"/usr/bin/");
             strcat(check,cmdsplit[0]);
             int cc=  access(check,X_OK);
            if(cc == 0) {
                execv(check,cmdsplit);
                wait(NULL);
              
                }
            }
        }

        

            
  return NULL;
}




