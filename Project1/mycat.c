#include <stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {

	FILE *cat;
	int i = 1;
	while( i<sizeof(*argv)) { //loops through the arguments

	if(argv[i] == NULL) {
		return(0); 

	}
	cat= fopen(argv[i], "r");
	if( cat == NULL ) {
		 printf("File does not exist\n");  // exits if file does not exist
    		fclose(cat);
			return(0); 
	}
	else {
		printf("----------------------DISPLAYING CONTENT IN %s-----------------------\n",argv[i]);
   		char txt= getc(cat);
    		while (txt != EOF) { //loops through to the end of file

        		printf("%c",txt); //prints line to the console
        		txt = fgetc(cat); //gets the nextline
		}
	fclose(cat);

	}
	i+=1;

	}


	return(0); 

}
